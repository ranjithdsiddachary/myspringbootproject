package com.basicprogram.basicprogram;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BasicprogramApplication {

	public static void main(String[] args) {
		SpringApplication.run(BasicprogramApplication.class, args);
		
	}

}
